<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
</head>
<body>

<?php
////// cria objeto
//$myObj->name = "John";
//$myObj->age = 30;
//$myObj->city = "New York";
////// transforma em json
//$myJSON = json_encode($myObj);
//echo $myJSON;
?>

<?php

// Atribui o conteúdo do arquivo para variável $arquivo
$arquivo = file_get_contents('cadastro.json');
 
// Decodifica o formato JSON e retorna um Objeto
$json = json_decode($arquivo);
 
// Loop para percorrer o Objeto
foreach($json as $registro):
    echo 'Código: ' . $registro->codigo . ' - Nome: ' . $registro->nome . ' - Telefone: ' . $registro->telefone . '<br>';
endforeach;
?>

</body>
</html>