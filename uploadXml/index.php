<?php
if (isset($_FILES['userfile'])) {
    $uploaddir = 'C:/xampp/htdocs/programacao/uploadXml/upload/'; // diretorio de upload dos arquivos
    $uploadfile = $uploaddir . $ext = date("Ymd-His") . '-' . strtolower($_FILES['userfile']['name']); // caminho do arquivo, diretorio, nome,data e extensão do arquivo

    if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) { // verifica se o arquivo foi movido com sucesso 
        echo "Possível ataque de upload de arquivo!\n";
    }
    /**  echo '<pre>debug:';
      print_r($_FILES);
      print "</pre>";    * */
    $xml = simplexml_load_file($uploadfile) or die("Error: Cannot create object"); // verifica se foi feito o carregamento do xml
    //print_r($xml);
    //echo $xml->channel[0]->title . ", ";
    //echo $xml->cliente[1]->email . ", , ";
    $servidor = 'localhost';
    $usuario = 'root';
    $senha = '';
    $banco = 'segura';
    $conn = new mysqli($servidor, $usuario, $senha, $banco);
    // connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    // end date conex BD
    foreach ($xml->children() as $dados) { //busca dados da empresa
        $idFuncionario = '';
        if ($dados->cnpjCliente) {
            /** 	echo 'identificacaoCliente:';
              echo $dados->cnpjCliente . ', ';
              echo $dados->codigoCliente . ', ';
              echo $dados->dataHoraEnvio . ',<br> ';* */
            $sqlSelectEmpresa = "SELECT * FROM empresas WHERE cnpj=" . $dados->cnpjCliente . " AND ativo = 'a'";
            $resultEmpresa = $conn->query($sqlSelectEmpresa);
            if ($resultEmpresa->num_rows > 0) {
                while ($row = $resultEmpresa->fetch_assoc()) {
                    $idEmpresa = $row["idEmpresa"];
                    "id: " . $row["idEmpresa"] . " - Name: " . $row["nomeEmpresa"] . "<br>";
                }
            }
        }
        foreach ($dados->children() as $dados2) { // busca dados do paciente
            if ($dados2->cpfPaciente) {
                $sqlSelectFunc = "SELECT * FROM funcionarios WHERE cpf=" . $dados2->cpfPaciente . " AND idEmpresa=" . $idEmpresa . "";
                $resultFunc = $conn->query($sqlSelectFunc);
                if ($resultFunc->num_rows > 0) {
                    while ($row = $resultFunc->fetch_assoc()) {
                        $idFuncionario = $row["idFuncionario"];
                        echo "idFuncionario: " . $row["idFuncionario"] . " - Name: " . $row["nomeFuncionario"] . "<br>";
                    }
                }
            }
            if ($idFuncionario) { //pega id do asos
               echo $sqlSelectAso = "SELECT idAso FROM asos WHERE idFuncionario=" . $idFuncionario . " AND idEmpresa=" . $idEmpresa . "";
                $resultAso = $conn->query($sqlSelectAso);
                if ($resultAso->num_rows > 0) {
                    while ($row = $resultAso->fetch_assoc()) {
                       $idAso = $row["idAso"];
                        //echo "<br>idAso: " . $row["idAso"] . "<br>";
                    }
                }
            }
            foreach ($dados2->children() as $dados3) { // busca dado do exame
                if ($dados3->codigoExame) {
//                    echo $dados3->codigoExame . ', ';
                    $sqlSelectExame = "SELECT idExame FROM exames WHERE idTuss=" . $dados3->codigoExame . "";
                    $resultExame = $conn->query($sqlSelectExame);
                }
                foreach ($dados3->children() as $dados4) {
                    foreach ($dados4->children() as $dados5) {
                        // echo $dados5->descricaoResultado . '<br> ';
                        if ($resultExame->num_rows > 0) {
                            while ($row = $resultExame->fetch_assoc()) {
                                echo '<br>'. $sqlUpdate = "UPDATE asosExames SET observacao='" . $dados5->descricaoResultado . "', resultadoNumero=" . $dados5->resultado . " WHERE idAso=".$idAso." AND idExame=" . $row["idExame"] . "";
                                if (!$conn->query($sqlUpdate) === TRUE) {
                                    echo "Error: " . $sqlUpdate . "<br>" . $conn->error;
                                }
                            }
                            if ($conn->query($sqlUpdate) === TRUE) {
                                echo "<br>dados atualizado com sucesso";
                            }
                        }
                    }
                }
            }
        }
        echo '<hr>';
    }
    $conn->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <style>
            .custom-file-input::-webkit-file-upload-button {
                visibility: hidden;
            }
            .custom-file-input::before {
                content: 'Anexar XML ';
                display: inline-block;
                background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
                border: 1px solid #999;
                border-radius: 3px;
                padding: 5px 8px;
                outline: none;
                white-space: nowrap;
                -webkit-user-select: none;
                cursor: pointer;
                text-shadow: 1px 1px #fff;
                font-weight: 700;
                font-size: 10pt;
            }
            .custom-file-input:hover::before {
                border-color: black;
            }
            .custom-file-input:active::before {
                background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
            }
        </style>
    </head>
    <body>
        <form action="index.php" method="post" enctype="multipart/form-data">
            <input type="file" name="userfile" id="fileToUpload"  class="custom-file-input">
            <input type="submit" value="Importar" name="submit">
        </form>
    </body>
</html>

